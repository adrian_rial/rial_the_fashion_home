$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $(".carousel").carousel({
        interval: 2000
    });

    $('#myModal').on('show.bs.modal', function(e) {
        console.log("Se ha mostrado del modal");
        $('#btnModal').removeClass('btn-secondary');
        $('#btnModal').addClass('btn-outline-secondary');
        $('#btnModal').prop('disabled', true);
    });
    $('#myModal').on('hide.bs.modal', function(e) {
        console.log("Se ha ocultado el modal");

        $('#btnModal').prop('disabled', false);
    });
});